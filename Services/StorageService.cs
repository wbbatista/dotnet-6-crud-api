﻿namespace WebApi.Services;

public class StorageService
{
    private List<string> _storageRepository { get; set; } = new List<string>();

    public void Add(string value)
    {
        _storageRepository.Add(value);
    }

    public List<string> GetAll()
    {
        return _storageRepository;
    }
}
